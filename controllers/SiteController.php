<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\AuthForm;
use app\components\AuthProcessor;
use splynx\helpers\ConfigHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    /**
     * This function will be triggered when user is successfuly authenticated using some oAuth client.
     *
     * @param yii\authclient\ClientInterface $client
     * @return boolean|yii\web\Response
     */
    public function successCallback($client) {
        (new AuthProcessor($client))->process();
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            return $this->authForm($user->identity->email, $user->identity->name, $user->identity->address);
        }

        return $this->render('index');
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Displays the next page after authorization.
     *
     * @param string $email
     * @param string $name
     * @param string $address
     * @return string|Response
     */
    public function authForm($email, $name, $address)
    {
        $model = new AuthForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {
            $idCustomer = $model->addCustomer();
            if ($idCustomer) {
                Yii::$app->session->setFlash('formSubmitted');
                //$url = "https://test2.createsite.biz/portal";
                $url = Yii::$app->request->hostInfo . "/portal";
                Yii::$app->user->logout();

                session_start();
                $_SESSION['splynx_customer_id'] = $idCustomer;

                return $this->redirect($url);
            } else {
                return $this->refresh();
            }
        } else {
            $model->email = $email;
            $model->name = $name;
            $model->address = $address;

            return $this->render('form', [
                'model' => $model,
            ]);
        }
    }
}
