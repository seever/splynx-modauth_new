<?php

$params = require __DIR__ . '/params.php';

return function ($params, $baseDir) {
    $db = require __DIR__ . '/db.php';
    return [
        'language' => 'uk-UA',//isset($params['language']) ? $params['language'] : 'en-US'
        'components' => [
            'authClientCollection' => [
                'class'   => \yii\authclient\Collection::className(),
                'clients' => [
                    // here is the list of clients you want to use
                    'facebook' => [
                        'class'        => 'yii\authclient\clients\Facebook',
                        'clientId'     => '',
                        'clientSecret' => '',
                    ],
                    'google' => [
                        'class'        => 'yii\authclient\clients\Google',
                        'clientId'     => '',
                        'clientSecret' => '',
                    ],
                    'twitter' => [
                        'class' => 'yii\authclient\clients\Twitter',
                        //'class' => 'yii\authclient\clients\TwitterOAuth2',
                        'attributeParams' => [
                            'include_email' => 'true'
                        ],
                        'consumerKey' => '',
                        'consumerSecret' => '',
                    ],
                ],
            ],
            'request' => [
                'baseUrl' => '/modauth'
            ],
            'user' => [
                'identityClass' => 'app\models\User',
                'enableAutoLogin' => true,
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'db' => $db,
        ],
        'params' => $params,
    ];
};