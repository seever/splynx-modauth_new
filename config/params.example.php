<?php

return [
    'api_domain' => '',
    'api_key' => '',
    'api_secret' => '',
    // Create payment method `CashDesk` at `Config / Finance / Payment methods` and enter it's id here
    // Or simply use existing method id
    'payment_method_id' => 1,
    // Group bank statements by `month` or `day`
    'bank_statements_group' => 'month',
    'cookieValidationKey' => '',
    'extend_customers_search' => true,

    // Set language
    'language' => 'en-US'
];