<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Перевірте Ваші дані';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-form">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('formSubmitted')): ?>

        <div class="alert alert-success">
            Дякую!
        </div>

    <?php else: ?>

        <p>Перед продовженням реєстрації перевірте ваші дані.</p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'address') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'password_repeat')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Зареєструватись', ['class' => 'btn btn-primary', 'name' => 'form-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
