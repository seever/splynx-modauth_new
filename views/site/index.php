<?php

/* @var $this yii\web\View */

$this->title = 'modAuth';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Реєстрація через соцмережі!</h1>
        <p class="lead">Тут Ви можете зареєструватись через соціальні мережі.</p>

        <div>
            <?= yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['site/auth']
            ]) ?>
        </div>
    </div>
</div>