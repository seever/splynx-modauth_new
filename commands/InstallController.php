<?php

namespace app\commands;

use splynx\base\BaseInstallController;

class InstallController extends BaseInstallController
{
    public function getAddOnTitle()
    {
        return 'ModAuth';
    }

    public function getModuleName()
    {
        return 'splynx_modauth_addon';
    }

    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'add'],
            ],
            [
                'controller' => 'api\admin\config\Mail',
                'actions' => ['index', 'add'],
            ],
        ];
    }

    public function getAdditionalFields()
    {
        return [
            [
                'main_module' => 'admins',
                'name' => 'modauth',
                'title' => 'ModAuth',
                'type' => 'boolean',
                'required' => false,
                'is_add' => true,
            ]
        ];
    }

    public function getEntryPoints()
    {
        return [];
    }
}
