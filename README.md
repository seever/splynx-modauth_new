Splynx ModAuth Add-on
======================

Splynx ModAuth Add-on based on [Yii2](http://www.yiiframework.com).

REQUIREMENTS
------------

Installed [Splynx ISP Framework](https://www.splynx.com)

MANUAL INSTALLATION
------------

Create Nginx config file `/etc/nginx/sites-available/splynx-modauth.addons`:
~~~
location /modauth
{
        try_files $uri $uri/ /modauth/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/splynx/splynx-addon-base.git
cd splynx-addon-base
composer global require "fxp/composer-asset-plugin:^1.2.0"
~~~

Add to composer.json
~~~
"yiisoft/yii2-authclient": "~2.1.0"
~~~

And
~~~
composer install
~~~


Install Splynx ModAuth Add-on:
~~~
cd /var/www/splynx/addons/
git clone https://bitbucket.org/seever/splynx-modauth.git
cd splynx-modauth
composer install
sudo chmod +x yii
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-modauth/web/ /var/www/splynx/web/modauth
~~~

Additional settings avaiable in `/var/www/splynx/addons/splynx-modauth/config/params.php`.

You can then access Splynx ModAuth Add-On through following URL:
~~~
http://YOUR_SPLYNX_DOMAIN/modauth
~~~