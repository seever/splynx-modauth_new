<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * AuthForm is the model behind the next auth form.
 */
class AuthForm extends Model
{
    public $name;
    public $email;
    public $address;
    public $password;
    public $password_repeat;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'email', 'address', 'password', 'password_repeat'], 'required'],
            ['email', 'email'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Паролі не збігаються"]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Прізвище і імя',
            'email' => 'E-mail',
            'address' => 'Адреса',
            'password' => 'Пароль',
            'password_repeat' => 'Підтвердіть пароль',
        ];
    }

    /**
     * Add a customer and send an email.
     * @return bool or id customer
     */
    public function addCustomer()
    {
        $params = array(
            'partner_id' => 1,
            'location_id' => 1,
            'name' => $this->name,
            'email' => $this->email,
            'category' => 'person',
            'street_1' => $this->address,
            'password' => $this->password,
        );
        $baseUrl = Yii::$app->request->hostInfo;
        $url = $baseUrl . "/api/1.0/admin/customers/customer";

        $response = $this->sendPost($url, $params);

        if ($response['success']) {
            $url = $baseUrl . "/api/1.0/admin/config/mail";
            $idCustomer = $response['object']['id'];
            //$_SESSION['splynx_customer_id'] = $id;

            $params = array(
                'customer_id' => $idCustomer,
                'type' => 'message',
                'recipient' => $this->email,
                'subject' => 'Реєстрація на порталі',
                'message' => 'Вітаю з успішною реєстрацією!'
            );

            $response = $this->sendPost($url, $params);

            return $idCustomer;
        } else {
            return false;
        }
    }

    /**
     * Send curl request to Splynx API
     *
     * @param string $url
     * @param array $params
     * @return array|boolean
     */
    public function sendPost ($url, $params)
    {
        $api_key = Yii::$app->params['api_key'];
        $api_secret = Yii::$app->params['api_secret'];

        $nonce = round(microtime(true) * 100);
        $signature = strtoupper(hash_hmac('sha256', $nonce . $api_key, $api_secret));
        $auth_data = array(
            'key' => $api_key,
            'signature' => $signature,
            'nonce' => $nonce++
        );
        $auth_string = http_build_query($auth_data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => array(
                'Content-type: multipart/form-data',
                'Authorization: Splynx-EA (' . $auth_string . ')',
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($err) {
            return array(
                'success' => false,
                'error' => $err,
            );
        } else {
            if ($response_code == 201) {
                return array(
                    'success' => true,
                    'object' => json_decode($response, true),
                );
            } else {
                return array(
                    'success' => false,
                    'object' => $response_code,
                );
            }
        }
    }

}
