<?php
namespace app\components;

use app\models\Auth;
use app\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthProcessor handles successful authentication via Yii auth component
 */
class AuthProcessor
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function process()
    {
        $attributes = $this->client->getUserAttributes();
        //var_dump($attributes); die;
        $id = ArrayHelper::getValue($attributes, 'id');

        switch ($this->client->getId()) {
            case 'facebook':
                $email = ArrayHelper::getValue($attributes, 'email');
                $name = ArrayHelper::getValue($attributes, 'name');
                $nickname = $email;
                break;
            case 'google':
                $email = ArrayHelper::getValue($attributes['emails'][0], 'value');
                $name = ArrayHelper::getValue($attributes['name'], 'givenName') . ' ' . ArrayHelper::getValue($attributes['name'], 'familyName');
                $nickname = $email;
                break;
            case 'twitter':
                $email = '';
                $name = ArrayHelper::getValue($attributes, 'screen_name');
                $nickname = ArrayHelper::getValue($attributes, 'name');
                break;
        }

        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id,
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /* @var User $user */
                $user = $auth->user;
                Yii::$app->user->login($user);
            } else { // signup
                $password = Yii::$app->security->generateRandomString(6);
                $user = new User([
                    'username' => $nickname,
                    'email' => $email,
                    'pass' => $password,
                    'name' => $name
                ]);

                $transaction = User::getDb()->beginTransaction();

                if ($user->save()) {
                    $auth = new Auth([
                        'user_id' => $user->id,
                        'source' => $this->client->getId(),
                        'source_id' => (string)$id,
                    ]);
                    if ($auth->save()) {
                        $transaction->commit();
                        Yii::$app->user->login($user);
                    } else {
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', 'Не вдалося зберегти обліковий запис {client}: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($auth->getErrors()),
                            ]),
                        ]);
                    }
                } else {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', 'Не вдається зберегти користувача: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($user->getErrors()),
                        ]),
                    ]);
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $this->client->getId(),
                    'source_id' => (string)$attributes['id'],
                ]);
                if ($auth->save()) {
                    /** @var User $user */
                    $user = $auth->user;
                    Yii::$app->getSession()->setFlash('success', [
                        Yii::t('app', '{client} обліковий запис.', [
                            'client' => $this->client->getTitle()
                        ]),
                    ]);
                } else {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', 'Не вдається пов\'язати обліковий запис {client}: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($auth->getErrors()),
                        ]),
                    ]);
                }
            } else { // there's existing auth
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app',
                        'Не вдається пов\'язати обліковий запис  {client} account. Є ще один користувач, який його використовує..',
                        ['client' => $this->client->getTitle()]),
                ]);
            }
        }
    }

}